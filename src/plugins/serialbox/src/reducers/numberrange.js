// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import {
    getPools,
    getPool,
    getRegion,
    getRegions,
    allocate,
    deleteRegion,
    deletePool
} from "../lib/serialbox-api";
import actions from "../actions/pools";

import {handleActions} from "redux-actions";
import {showMessage} from "../../../../lib/message";
import serverActions from "actions/serversettings";
import base64 from "base-64";
import {Parser} from "json2csv";
import jsonToXML from "jsontoxml";
import {pluginRegistry} from "plugins/pluginRegistration";
import {setServerState} from "lib/reducer-helper";
import { loadRule } from "../../../capture/src/reducers/capture";
export const initialData = () => ({
    servers: {},
    region: {},
    currentRegions: [],
    pool: {}
});

export const loadResponseRules = async (server, response) => {
    try {
        sessionStorage.setItem("loadingRR", true);
        let responseRules = await pluginRegistry
            .getServer(server.serverID)
            .fetchListAll("serialbox_response_rules_list", {}, []);
        // poor-man's matching alg.
        let poolsMap = {};
        response.results.forEach(pool => {
            poolsMap[pool.id] = pool;
            pool.response_rules = [];
        });
        if (responseRules && responseRules.length > 0) {
            responseRules.forEach(responseRule => {
                try {
                    poolsMap[responseRule.pool].response_rules.push(responseRule);
                } catch (e) {
                    // ignore responseRule that don't have a pool set.
                    console.log("A response rule appears to not have a pool/rule assigned.");
                    console.log(e);
                }
            });
        }
        if (response) {
            setTimeout(()=> {
                sessionStorage.setItem("loadingRR", false);
              }, []);
            return response
        }
        return response
    } catch (e) {
        if (response) {
            console.log("Error inside response!")
            setTimeout(()=> {
                sessionStorage.setItem("loadingRR", false);
              }, []);
            return response
        }
        return response
    }
};

export const loadPools = server => {
    return dispatch => {
        getPools(server).then(async pools => {
            // load response rules, if available.
            pools = await loadResponseRules(server, pools);
            dispatch({
                type: actions.loadPools,
                payload: {
                    [server.serverID]: {pools: pools, server: server}
                }
            });
        });
    };
};

export const loadPool = (server, poolName) => {
    return dispatch => {
        getPool(server, poolName)
        .then(async pool => {
            dispatch({
                type: actions.loadPool,
                payload: pool
            });
            getRegions(server, pool).then(regions => {
                dispatch({
                    type: actions.loadRegions,
                    payload: regions
                });
            });
        });
        
    };
};

export const getResponseRules = pool => {
    return dispatch => {

    }
}

export const loadRegion = (server, regionName) => {
    return dispatch => {
        getRegion(server, regionName).then(region => {
            dispatch({
                type: actions.loadRegion,
                payload: region
            });
        });
    };
};

export const loadRegions = (server, pool) => {
    return dispatch => {
        // first get all pools again to refresh pool list.
        getPools(server)
            .then(async pools => {
                pools = await loadResponseRules(server, pools);
                dispatch({
                    type: actions.loadPools,
                    payload: {
                        serverID: server.serverID,
                        pools: pools,
                        server: server
                    }
                });

                // second get region for given pool (updated.)
                let updatedPool = pools.find(aPool => {
                    return aPool.machine_name === pool.machine_name;
                });
                getRegions(server, updatedPool).then(regions => {
                    dispatch({
                        type: actions.loadRegions,
                        payload: regions
                    });
                });
            })
            .catch(e => {
                showMessage({type: "error", msg: e});
            });
    };
};
export const loadRegionsForNumberPool = (server, pool) => {
    return dispatch => {
        // first get all pools again to refresh pool list.
        loadPool()
            .then(async pools => {
                // second get region for given pool (updated.)
                let updatedPool = pools.find(aPool => {
                    return aPool.machine_name === pool.machine_name;
                });
                getRegions(server, updatedPool).then(regions => {
                    dispatch({
                        type: actions.loadRegions,
                        payload: regions
                    });
                });
            })
            .catch(e => {
                showMessage({type: "error", msg: e});
            });
    };
};
export const loadExactRegionsForNumberPool = (server, pool) => {
    return dispatch => {
    getPool(server, pool.machine_name)
    .then(async pool => {
        dispatch({
            type: actions.loadPool,
            payload: pool
        });
    })
    getRegions(server, pool)
    .then(async regions => {
        dispatch({
            type: actions.loadRegions,
            payload: regions
        });
    });
};
};
export const deleteARegion = (server, pool, region) => {
    return dispatch => {
        deleteRegion(server, region)
            .then(response => {
                if (!response.ok) {
                    if (response.status === 403 || response.status === 401) {
                        pluginRegistry.getHistory().push("/access-denied");
                        return;
                    } else {
                        throw new Error(response);
                    }
                }
                if (response.ok && response.status === 204) {
                    dispatch(loadRegions(server, pool));
                    showMessage({
                        id: "plugins.numberRange.regionDeletedSuccessfully",
                        type: "warning"
                    });
                }
            })
            .catch(error => {
                showMessage({type: "error", msg: error.detail});
            });
    };
};
export const deleteARegionOfNumberPool = (server, pool, region) => {
    return dispatch => {
        deleteRegion(server, region)
            .then(response => {
                if (!response.ok) {
                    if (response.status === 403 || response.status === 401) {
                        pluginRegistry.getHistory().push("/access-denied");
                        return;
                    } else {
                        throw new Error(response);
                    }
                }
                if (response.ok && response.status === 204) {
                    dispatch(loadPool(server, pool.machine_name))
                    // dispatch(loadRegions(server, pool));
                    showMessage({
                        id: "plugins.numberRange.regionDeletedSuccessfully",
                        type: "warning"
                    });
                }
            })
            .catch(error => {
                showMessage({type: "error", msg: error.detail});
            });
    };
};
export const deleteAPool = (server, pool) => {
    return dispatch => {
        deletePool(server, pool)
            .then(response => {
                if (response.status === 403 || response.status === 401) {
                    pluginRegistry.getHistory().push("/access-denied");
                    return;
                }
                if (response.ok && response.status === 204) {
                    dispatch(loadPoolList(server));
                    showMessage({type: "warning", msg: "Pool deleted successfully"});
                }
            })
            .catch(error => {
                showMessage({type: "error", msg: error.detail});
            });
    };
};

export const deleteResponseRule = (server, responseRule, page) => {
    return async dispatch => {
        pluginRegistry
            .getServer(server.serverID)
            .getClient()
            .then(client => {
                client.apis.serialbox
                    .serialbox_response_rules_delete(responseRule)
                    .then(result => {
                        return dispatch(loadPoolList(server, null, page, null, 0), loadRule(server, responseRule.rule));
                    })
                    .catch(e => {
                        showMessage({
                            type: "error",
                            msg: "An error occurred while attempting to delete response rule"
                        });
                    });
            });
    };
};

const generateFile = (server, pool, exportType, data, size) => {
    if (
        typeof data === "object" &&
        data.numbers &&
        typeof data.numbers === "string"
    ) {
        // parse array of numbers.
        data.numbers = JSON.parse(data.numbers.replace(/'/g, '"'));
    }
    let encodedResult = "";
    // download the result.
    if (exportType === "json") {
        encodedResult = base64.encode(JSON.stringify(data));
    } else if (exportType === "csv") {
        let csvParser = new Parser();
        let numberMap = data.numbers.map(number => {
            return {
                numbers: number
            };
        });
        const csv = csvParser.parse(numberMap);
        encodedResult = base64.encode(csv);
    } else if (exportType === "xml") {
        encodedResult = base64.encode(data);
    }
    let link = document.createElement("a");
    let regionName = data.region || 0;
    link.download = `${pool.machine_name}-${regionName}-${size}.${exportType}`;
    link.href = `data:application/octet-stream;charset=utf-8;content-disposition:attachment;base64,${encodedResult}`;
    link.click();
};
export const setAllocation = (server, pool, value, exportType) => {
    return dispatch => {
        allocate(server, pool, value, exportType).then(data => {
            // console.info('dispatch called...')
            if (typeof data === "object") {
                // let's take a look at the data.
                if (data.detail) {
                    // looks like an error.
                    showMessage({type: "error", msg: data.detail});
                } else if (data.fulfilled === true) {
                    showMessage({
                        id: "plugins.numberRange.allocatedSuccess",
                        type: "success",
                        values: {size: data.size_granted, regionName: data.region}
                    });
                }
            } else {
                showMessage({
                    id: "plugins.numberRange.allocatedSuccess",
                    type: "success",
                    values: {size: value, regionName: ""}
                });
            }
            dispatch({type: actions.allocate, payload: data});
            // reload regions.
            getRegions(server, pool).then(regions => {
                dispatch({
                    type: actions.loadRegions,
                    payload: regions
                });
            });
            setTimeout(() => {
                try {
                    if (data.size_granted || typeof data === "string") {
                        generateFile(server, pool, exportType, data, value);
                    }
                } catch (error) {
                    showMessage({
                        id: "plugins.numberRange.errorFailedToGenerateFile",
                        type: "danger"
                    });
                }
            }, 500);
        });
    };
};

export default handleActions(
    {
        [actions.loadPools]: (state, action) => {
            return setServerState(state, action.payload.serverID, {
                server: action.payload.server,
                pools: action.payload.pools,
                count: action.payload.count,
                next: action.payload.next
            });
        },
        [actions.loadPool]: (state, action) => {
            return {
                ...state,
                pool: action.payload,
            };
        },
        [actions.loadRegion]: (state, action) => {
            return {
                ...state,
                region: action.payload
            };
        },
        [actions.loadRegions]: (state, action) => {
            return {
                ...state,
                currentRegions: action.payload
            };
        },
        [actions.allocate]: (state, action) => {
            // reload pools after
            //action.asyncDispatch(loadPools(action.payload));
            // don't pass allocation since it fails in redux.
            return {
                ...state
            };
        },

        [serverActions.serverUpdated]: (state, action) => {
            // we want to reload pools when new server is saved.
            /*action.asyncDispatch(
              loadPools(pluginRegistry.getServer(action.payload))
            );*/
            return {
                ...state
            };
        }
    },
    {}
);

export const loadResponseRulesForNumberPool = async (server, response, poolID) => {
    console.log(server, response, poolID);
    try {
        let responseRules = await pluginRegistry
            .getServer(server.serverID)
            .fetchListAll("serialbox_response_rules_pool_list", {pool_id: poolID}, []);
        let poolsMap = {};
        response.pools.forEach(pool => {
            poolsMap[pool.id] = pool;
            pool.response_rules = [];
        });
        if (responseRules && responseRules.length > 0) {
            responseRules.forEach(responseRule => {
                try {
                    poolsMap[responseRule.pool].response_rules.push(responseRule);
                } catch (e) {
                    // ignore responseRule that don't have a pool set.
                    console.log("A response rule appears to not have a pool/rule assigned.");
                    console.log(e);
                }
            });
        }
        return response
        
    } catch (e) {
        return response
    }
};

export const loadPoolList = (server, search, page, ordering, poolID) => {
    const params = {};
    if (search) {
        params.search = search;
    }
    if (page) {
        params.page = page;
    }
    if (ordering) {
        params.ordering = ordering;
    }
    if (poolID) {
        params.poolID = poolID;
    }
    return async dispatch => {
        sessionStorage.setItem("loadingRR", true);
        let serverObject = pluginRegistry.getServer(server.serverID);
        let response_pools = null;
        let pools = null;
        serverObject
            .fetchPageList("serialbox_pools_list", params, [])
            .then(async response => {
                response = await loadResponseRulesForNumberPool(server, response, poolID=poolID);
                sessionStorage.setItem("loadingRR", false);
                return dispatch({
                    type: actions.loadPools,
                    payload: {
                        serverID: server.serverID,
                        server: server,
                        pools: response.results,
                        count: response.count,
                        next: response.next
                    }
                });
            })
            .catch(e => {
                sessionStorage.setItem("loadingRR", false);
                showMessage({
                    type: "error",
                    id: "plugins.masterData.errorFetchPools",
                    values: {error: e}
                });
            });
    };
};


export const loadResponserulesForPool = (server, poolID) => {
    return async dispatch => {
        let serverObject = pluginRegistry.getServer(server.serverID);
        sessionStorage.setItem("loadingRR", true);
        serverObject
            .fetchPageList("serialbox_pools_list", params, [])
            .then(async response => {
                sessionStorage.setItem("loadingRR", false);
                response = await loadResponseRulesForNumberPool(server, response, poolID=poolID);
                return dispatch({
                    type: actions.loadPools,
                    payload: {
                        serverID: server.serverID,
                        server: server,
                        pools: response.results,
                        count: response.count,
                        next: response.next
                    }
                })
            })
            .catch(e => {
                sessionStorage.setItem("loadingRR", false);
                showMessage({
                    type: "error",
                    id: "plugins.masterData.errorFetchPools",
                    values: {error: e}
                });
            });
    };
};